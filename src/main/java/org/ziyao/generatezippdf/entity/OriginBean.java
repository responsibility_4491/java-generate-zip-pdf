package org.ziyao.generatezippdf.entity;

import lombok.Data;

import java.util.List;

@Data
public class OriginBean {

    private Integer code;
    private String message;

    private List<DeptBean> data;
}
