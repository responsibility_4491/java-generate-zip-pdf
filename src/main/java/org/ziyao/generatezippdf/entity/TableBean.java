package org.ziyao.generatezippdf.entity;

import lombok.Data;

@Data
public class TableBean {

    private String name;
    private SixBean value;
}
