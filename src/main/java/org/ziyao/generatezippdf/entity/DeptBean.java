package org.ziyao.generatezippdf.entity;

import lombok.Data;

import java.util.List;

@Data
public class DeptBean {

    private Integer id;
    private String orgId;
    private String name;
    private Float total;
    private String zpSzhqk; // 数字化情况
    private String zpSjgx; // 数据共享及数据分析工作开展情况
    private String zpCzwt; // 存在的问题
    private String xybgzsl; // 下一步工作思路

    private List<TableBean> report;
}
