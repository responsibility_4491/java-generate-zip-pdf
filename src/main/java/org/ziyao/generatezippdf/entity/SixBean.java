package org.ziyao.generatezippdf.entity;

import lombok.Data;

@Data
public class SixBean {
    // 字段影响最终表格中的顺序，慎重改动
    private Float szhqk;
    private Float sjgx;
    private Float sjyy;
    private Float sjfx;
    private Float ai;
    private Float sjaq;
}
