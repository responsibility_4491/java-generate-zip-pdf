package org.ziyao.generatezippdf.service;

import com.itextpdf.io.font.PdfEncodings;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class FontService {

    private final ConcurrentHashMap<String, PdfFont> fontCache = new ConcurrentHashMap<>();

    public PdfFont getFont(String fontName, String fontPath) throws IOException {
        return fontCache.computeIfAbsent(fontName, k -> {
            try {
                return PdfFontFactory.createFont(fontPath, PdfEncodings.IDENTITY_H);
            } catch (IOException e) {
                throw new RuntimeException("Failed to load font: " + fontName, e);
            }
        });
    }
}